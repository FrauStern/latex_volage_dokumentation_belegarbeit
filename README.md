# Report Template
Dies ist ein Template-Vorschlag zur Dokumentation von der IKT-Belegarbeit. Siehe "report.pdf" um den aktuellen Struktur-Vorschlag für die Dokumentation zu sehen. 

## General Requirements
To compile the template the following software is required:
* lualatex (TeX)
* biber (citation)

## Compilation
The templates have been tested on Linux (Ubuntu), Windows and macOS with the latest version of the software and should be compatible with any IDE. For instructions how to get lualatex with biber installed and running, please refer to your operating system's manual/online help.

__Generate / Update PDF__  

In a basic shell, you have to perform three steps to generate the output. First compile the main.tex

```
lualatex report.tex
```

Afterwards generate the references

```
biber report.bcf
```

As the last step, the tex file has to be recompiled for linking to the references

```
lualatex report.tex
```
